package mx.tec.sep23;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private ArrayList<String> goodbois;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        // data
        goodbois = new ArrayList<>();
        goodbois.add("Fido");
        goodbois.add("Fifi");
        goodbois.add("Kaiser");
        goodbois.add("Clementina");
        goodbois.add("Firulais");

        // translator - Adapter
        DogAdapter adapter = new DogAdapter(goodbois, this);

        // recycler view (GUI)
        // layout manager - LinearLayoutManager, GridLayoutManager
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {

        int pos = recyclerView.getChildLayoutPosition(view);
        Toast.makeText(this, goodbois.get(pos), Toast.LENGTH_SHORT).show();
    }
}