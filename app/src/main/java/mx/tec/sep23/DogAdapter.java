package mx.tec.sep23;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DogAdapter extends RecyclerView.Adapter<DogAdapter.DogViewHolder> {

    public static class DogViewHolder extends RecyclerView.ViewHolder {

        // View Holder - class in charge of dealing with a view
        // corresponding to a single element (row)

        public TextView text1, text2;

        public DogViewHolder(@NonNull View itemView) {
            super(itemView);

            text1 = itemView.findViewById(R.id.textView3);
            text2 = itemView.findViewById(R.id.textView4);
        }
    }

    // constructor
    // we ALWAYS need a data source

    private ArrayList<String> doggies;
    private View.OnClickListener listener;

    public DogAdapter(ArrayList<String> doggies, View.OnClickListener listener) {
        this.doggies = doggies;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // parse / translate from XML to Java Object
        View v = (View) LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row, parent, false);

        // add behaviour to button
        Button b = v.findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.wtf("BOTON INTERNO", "HOLA!!!");
            }
        });

        v.setOnClickListener(listener);

        DogViewHolder dvh = new DogViewHolder(v);
        return dvh;
    }

    @Override
    public void onBindViewHolder(@NonNull DogViewHolder holder, int position) {

        holder.text1.setText(doggies.get(position));
        holder.text2.setText(doggies.get(position));
    }

    @Override
    public int getItemCount() {
        return doggies.size();
    }


}
